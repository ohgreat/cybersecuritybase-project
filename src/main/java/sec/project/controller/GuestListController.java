package sec.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import sec.project.config.SignupService;

@Controller
public class GuestListController {

    @Autowired
    private SignupService signupService;

    @RequestMapping(value = "/guestList", method = RequestMethod.GET)
    public String loadGuestList(Model model) {
        model.addAttribute("guests", signupService.list());
        return "guest_list";
    }
}
