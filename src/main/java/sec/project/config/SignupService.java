package sec.project.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sec.project.domain.Signup;
import sec.project.repository.SignupRepository;

import java.util.List;

@Service
public class SignupService {

    @Autowired
    private SignupRepository signupRepository;

    public List<Signup> list() {
        return signupRepository.findAll();
    }

}
