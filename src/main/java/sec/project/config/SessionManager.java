package sec.project.config;

import org.apache.catalina.session.StandardManager;

public class SessionManager extends StandardManager {
    private int id = 0;

    @Override
    protected String generateSessionId() {
        super.generateSessionId();
        return Integer.toString(id++);
    }
}
